terraform {
  backend "s3" {
    bucket         = "dvodopianov-lambda-state-bucket"
    key            = "terraform.tfstate"
    region         = "eu-central-1"
    dynamodb_table = "dvodopianov-lambda-lock"
    encrypt        = true
  }
}


provider "aws" {
  region = "eu-central-1"
}

locals {
  project_name  = "dvodopianov_lambda"
  myregion      = "eu-central-1"
  accountId     = "951487553564"
  endpoint_path = "test"
}


module "S3" {
  source = "../modules/S3"
}

module "IAM" {
  source = "../modules/IAM"

  bucket_arn  = module.S3.bucket_arn
  name_object = module.S3.name_object
}

module "API_Gateway" {
  source = "../modules/Api_Gateway"

  endpoint_path = local.endpoint_path
}

module "Lambda" {
  source = "../modules/Lambda"

  bucket_arn                         = module.S3.bucket_arn
  name_object                        = module.S3.name_object
  iam_role_arn                       = module.IAM.iam_role_arn
  project_name                       = local.project_name
  aws_api_gateway_rest_api_id        = module.API_Gateway.api_gateway_rest_api_id
  aws_api_gateway_resource_id        = module.API_Gateway.aws_api_gateway_resource_id
  aws_api_gateway_method_http_method = module.API_Gateway.aws_api_gateway_method_http_method
  myregion                           = local.myregion
  accountId                          = local.accountId
  api_gateway_rest_api_body          = module.API_Gateway.api_gateway_rest_api_body
}
