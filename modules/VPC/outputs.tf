output "vpc_id" {
  description = "The ID of the VPC"
  value       = aws_vpc.vpc.id
}

output "privat_subnet_1_id" {
  value = aws_subnet.private_1.id
}

output "privat_subnet_2_id" {
  value = aws_subnet.private_2.id
}

output "public_subnet_1_id" {
  description = "ID of public subnet"
  value       = aws_subnet.public_1.id
}

output "public_subnet_2_id" {
  description = "ID of public subnet"
  value       = aws_subnet.public_2.id
}
