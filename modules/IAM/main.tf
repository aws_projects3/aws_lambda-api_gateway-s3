data "aws_iam_policy_document" "policy" {
  statement {
    sid    = "1"
    effect = "Allow"
    actions = [
    "s3:GetObject",
    ]
    resources = [
      "${var.bucket_arn}/${var.name_object}"
    ]
  }
}


resource "aws_iam_role" "lambda" {
  name               = "lambda_role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Principal = {
          Service = "lambda.amazonaws.com"
        }
      }
    ]
  })
}


resource "aws_iam_policy" "custom_policy" {
  name        = "CustomLambdaPolicy"
  description = "Custom policy for Lambda function"
  policy      = data.aws_iam_policy_document.policy.json
}

resource "aws_iam_role_policy_attachment" "attach_policy" {
  policy_arn = aws_iam_policy.custom_policy.arn
  role       = aws_iam_role.lambda.name
}