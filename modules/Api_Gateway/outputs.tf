output "api_gateway_rest_api_id" {
  value       = aws_api_gateway_rest_api.example.id
  description = "IAM Role ARN"
}

output "api_gateway_rest_api_body" {
  value       = aws_api_gateway_rest_api.example.body
  description = "IAM Role ARN"
}

output "aws_api_gateway_resource_id" {
  value       = aws_api_gateway_resource.example_resource.id
  description = "IAM Role ARN"
}

output "aws_api_gateway_method_http_method" {
  value       = aws_api_gateway_method.example_method.http_method
  description = "IAM Role ARN"
}