resource "aws_s3_bucket" "artifacts" {
  bucket = "dvodopianov-lambda"

  tags = {
    Name      = "dvodopianov-lambda-bucket"
    Resource  = "lambda"
    Terraform = true
  }
}

resource "aws_s3_bucket_ownership_controls" "example" {
  bucket = aws_s3_bucket.artifacts.id
  rule {
    object_ownership = "BucketOwnerPreferred"
  }
}

resource "aws_s3_bucket_acl" "example_bucket_acl" {
  depends_on = [aws_s3_bucket_ownership_controls.example]

  bucket = aws_s3_bucket.artifacts.id
  acl    = "private"
}

# Upload an object
resource "aws_s3_object" "index_html" {
  bucket = aws_s3_bucket.artifacts.id
  key    = "index.html"
  acl    = "private"
  source = "../modules/S3/files/index.html"
  etag   = filemd5("../modules/S3/files/index.html")
}
