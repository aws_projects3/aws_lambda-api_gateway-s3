output "bucket_arn" {
  value       = aws_s3_bucket.artifacts.arn
  description = "Bucket ARN"
}

output "name_object" {
  value       = aws_s3_object.index_html.key
  description = "Obdject Key"
}