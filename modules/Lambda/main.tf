data "archive_file" "lambda" {
  type        = "zip"
  source_file = "${path.module}/files/file_for_lambda.py"
  output_path = "${path.module}/files/lambda_function_payload.zip"
}

resource "aws_lambda_function" "example" {
  filename = "${path.module}/files/lambda_function_payload.zip"

  function_name = var.project_name
  handler       = "file_for_lambda.lambda_handler"
  role          = var.iam_role_arn

  source_code_hash = data.archive_file.lambda.output_base64sha256
  runtime          = "python3.9"
}

resource "aws_api_gateway_integration" "integration" {
  rest_api_id             = var.aws_api_gateway_rest_api_id
  resource_id             = var.aws_api_gateway_resource_id
  http_method             = var.aws_api_gateway_method_http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.example.invoke_arn
}

resource "aws_lambda_permission" "apigw_lambda" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.example.function_name
  principal     = "apigateway.amazonaws.com"
  #source_arn    = "arn:aws:execute-api:${var.myregion}:${var.accountId}:${var.aws_api_gateway_rest_api_id}/*/${var.aws_api_gateway_method_http_method}${aws_api_gateway_resource.example_resource.path}"
}

resource "aws_api_gateway_deployment" "example" {
  rest_api_id = "${var.aws_api_gateway_rest_api_id}"
  triggers = {
    redeployment = sha1(jsonencode("${var.api_gateway_rest_api_body}"))
  }
  lifecycle {
    create_before_destroy = true
  }
  depends_on = [aws_api_gateway_integration.integration]
}

resource "aws_api_gateway_stage" "example" {
  deployment_id = aws_api_gateway_deployment.example.id
  rest_api_id   = var.aws_api_gateway_rest_api_id
  stage_name    = "dev"
}