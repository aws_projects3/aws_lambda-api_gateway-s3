variable "bucket_arn" {
  type = string
}

variable "name_object" {
  type = string
}

variable "iam_role_arn" {
  type = string
}

variable "project_name" {
  type = string
}

variable "aws_api_gateway_rest_api_id" {
  type = string
}

variable "aws_api_gateway_resource_id" {
  type = string
}

variable "aws_api_gateway_method_http_method" {
  type = string
}

variable "myregion" {
  type = string
}

variable "accountId" {
  type = string
}

variable "api_gateway_rest_api_body" {
  type = string
}