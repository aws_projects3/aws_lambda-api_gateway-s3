import boto3

def lambda_handler(event, context):
    s3_bucket_name = 'dvodopianov-lambda'
    index_file_key = 'index.html'

    # Create an S3 client
    s3_client = boto3.client('s3')

    try:
        # Get the 'index.html' file content from the S3 bucket
        response = s3_client.get_object(Bucket=s3_bucket_name, Key=index_file_key)
        html_content = response['Body'].read().decode('utf-8')

        return {
            'statusCode': 200,
            'headers': {
                'Content-Type': 'text/html'
            },
            'body': html_content
        }

    except Exception as e:
        return {
            'statusCode': 500,
            'body': str(e)
        }